import "./App.css";
import { createGraphiQLFetcher } from "@graphiql/toolkit";
import { GraphiQL } from "graphiql";
import { AmplifyAuthenticator, AmplifySignOut } from "@aws-amplify/ui-react";
import "graphiql/graphiql.css";
import Amplify, { Auth } from "aws-amplify";
import { useState } from "react";

Amplify.configure({
  Auth: {
    region: `${process.env.REACT_APP_REGION}`,
    userPoolId: process.env.REACT_APP_USERPOOLID,
    userPoolWebClientId: process.env.REACT_APP_CLIENTID,
    mandatorySignIn: true,
  },
});

function App() {
  console.log(process.env, "region");
  const [accessToken, setAccessToken] = useState("");

  async function getAccessToken() {
    try {
      const session = await Auth.currentSession();
      const token = session.accessToken.jwtToken;
      setAccessToken(token);
    } catch (error) {
      console.log(error);
    }
  }
  console.log("userId", accessToken.split(" ")[1]);
  // call getAccessToken function
  getAccessToken();

  const fetcher = createGraphiQLFetcher({
    url: process.env.REACT_APP_GRAPHQL_ENDPOINT,
    headers: {
      // authMode: "AWS_LAMBDA",
      authorization: `Bearer ${accessToken}`,
    },
  });
  return (
    <div>
      <AmplifyAuthenticator>
        <GraphiQL fetcher={fetcher} />,
      </AmplifyAuthenticator>
      <AmplifySignOut />
    </div>
  );
}

export default App;
