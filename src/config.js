export const AmplifyConfig = {
  Auth: {
    region: process.env.REGION,
    userPoolId: process.env.USERPOOLID,
    userPoolWebClientId: process.env.CLIENTID,
  },
};
